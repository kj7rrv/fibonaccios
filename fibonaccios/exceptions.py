class CircuitOSError(Exception):
    pass


class NoSuchProcess(CircuitOSError):
    pass


class AppNotFound(CircuitOSError):
    pass


class InvalidApp(AppNotFound):
    pass


class NotConsoleController(CircuitOSError):
    pass
