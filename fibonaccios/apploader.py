from fibonaccios.exceptions import *

_cache = {}

def get_app(app):
    try:
        return _cache[app]
    except KeyError:
        try:
            module = __import__(f'/bin/{app}')
        except ImportError:
            raise AppNotFound(app)

        try:
            app_class = module.get_app_class(App)
        except AttributeError:
            raise InvalidApp(app)

        _cache[app] = app_class

        return app_class
