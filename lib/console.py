def read(console, buffer):
    char = console.read(1)
    if char == b'\x08':
        if buffer:
            console.write(b'\x08 \x08')
            buffer.pop()
        return
    elif char == b'\n':
        console.write(b'\n')
        data = b''.join(buffer)
        buffer.clear()
        return data
    elif char == b'\x04' and not buffer:
        raise EOFError()
    elif len(char) == 1:
        console.write(char)
        buffer.append(char)
    else:
        return None


def write_str(console, string, encoding="utf-8"):
    console.write(string.encode(encoding))

def print(console, *values, sep=" ", end="\n"):
    write_str(console, sep.join(str(value) for value in values) + end)
