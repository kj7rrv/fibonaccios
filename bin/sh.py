import fibonaccios.exceptions
import shlex
import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            self.jump('prompt')

        def F_prompt(self):
            console.print(self.console, 'FibonacciOS sh # ', end="")
            self.store['buf'] = []
            self.jump('input')

        def F_input(self):
            try:
                raw_line = console.read(self.console, self.store['buf'])
            except EOFError:
                self.exit()
                return

            if raw_line is not None:
                line = shlex.split(raw_line.decode('ascii'))
                if line:
                    try:
                        if line[0] == 'bg':
                            self.launch(*line[1:])
                        elif line[0] == 'cd':
                            self.chdir(line[1])
                        elif line[0] == 'exit':
                            self.exit()
                        else:
                            pid = self.launch(*line)
                            self.wait(pid)
                            self.transfer_console(pid)
                    except fibonaccios.exceptions.AppNotFound as e:
                        console.print(self.console, 'error: app not found')
                self.jump('prompt')

    return App
