import os
import re
import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            pattern = self.argv[1].encode("utf-8")
            files = self.argv[2:]

            if files:
                prefix = len(files) > 1
                for file in files:
                    try:
                        with open(file, "rb") as f:
                            for line in f.readlines():
                                if re.search(pattern, line):
                                    self.console.write((f"{file}:".encode("utf-8") if prefix else b"") + line)
                    except OSError as e:
                        try:
                            os.listdir(file)
                            console.print(self.console, f"grep: {file}: Is a directory")
                        except OSError:
                            console.print(self.console, f"grep: {file}: No such file or directory")
                self.exit()
            else:
                self.store["pattern"] = pattern
                self.store["buffer"] = []
                self.jump("loop")
            

        def F_loop(self):
            try:
                line = console.read(self.console, self.store["buffer"])
            except EOFError:
                self.exit()
                return

            if line and re.search(self.store["pattern"], line):
                self.console.write(line + b"\n")

    return App
