import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            apps = self.os.apps
            for pid, app in apps.items():
                args = app.argv[1:]
                name = app.name
                console.print(self.console, f'{pid}	{name} {args}')
            self.exit()

    return App
