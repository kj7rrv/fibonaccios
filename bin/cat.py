import os
import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            for file in self.argv[1:]:
                try:
                    with open(file, "rb") as f:
                        self.console.write(f.read())
                except OSError as e:
                    try:
                        os.listdir(file)
                        console.print(self.console, f"cat: {file}: Is a directory")
                    except OSError:
                        console.print(self.console, f"cat: {file}: No such file or directory")

            self.exit()

    return App
