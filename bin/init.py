import shlex
import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            with open('/etc/init/start') as f:
                lines = f.readlines()
            for line in lines:
                if line.strip():
                    last_pid = self.launch(*shlex.split(line))

            self.transfer_console(last_pid)
            self.next_function = 'loop'

        def F_loop(self):
            if len(self.os.apps) == 1:
                console.print(self.console, "All processes have quit.")
                self.jump("end")

        def F_end(self):
            pass

    return App
