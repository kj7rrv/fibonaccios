import os
import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            if len(self.argv) == 1:
                path = "."
            else:
                path = self.argv[1]

            try:
                for file in os.listdir(path):
                    console.print(self.console, file)
            except OSError as e:
                try:
                    os.stat(path)
                    console.print(self.console, path)
                except OSError:
                    console.print(self.console, f"ls: cannot access '{path}': No such file or directory")

            self.exit()

    return App
