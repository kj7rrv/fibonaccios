import os
import console

def get_app_class(template):
    class App(template):
        def F_main(self):
            console.print(self.console, os.getcwd())
            self.exit()

    return App
